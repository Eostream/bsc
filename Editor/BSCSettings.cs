﻿/*Made by Eostream : Michaël Cossu - 17-12-2018*/
using UnityEngine;
using UnityEditor;

namespace BSC
{
    public static class BSCSettings
    {
        public enum ShiftMode
        {
            Fixed = 0,
            Factor = 1
        }

        private const string ACTIVATED_REG_KEY = "BSC::Activated";
        private const string DISPLAY_SETTINGS_BUTTON_REG_KEY = "BSC::DisplaySettingsButton";
        private const string DISPLAY_SLIDER_REG_KEY = "BSC::DisplaySlider";

        private const string MIN_SPEED_REG_KEY = "BSC::MinSpeed";
        private const string MAX_SPEED_REG_KEY = "BSC::MaxSpeed";

        private const string SHIFT_MODE_REG_KEY = "BSC::ShiftMode";

        private const string SHIFT_FIXED_REG_KEY = "BSC::ShiftFixed";
        private const string SHIFT_FACTOR_REG_KEY = "BSC::ShiftFactor";

        private const string CAMERA_SPEED_REG_KEY = "BSC::Camera_Speed";
        private const string WHEEL_SPEED_FACTOR_REG_KEY = "BSC::WheelSpeedFactor";

        private const string USE_INERTIA_REG_KEY = "BSC::UseInertia";
        private const string INERTIA_LENGTH_REG_KEY = "BSC::InertiaLength";

        private const string FLY_FORWARD_REG_KEY = "BSC::Fly_Forward";
        private const string FLY_BACKWARD_REG_KEY = "BSC::Fly_Backward";
        private const string FLY_RIGHT_REG_KEY = "BSC::Fly_Right";
        private const string FLY_LEFT_REG_KEY = "BSC::Fly_Left";
        private const string FLY_UP_REG_KEY = "BSC::Fly_Up";
        private const string FLY_DOWN_REG_KEY = "BSC::Fly_Down";

        private const string SLIDER_ANCHOR_REG_KEY = "BSC::Slider_Anchor";

        private const string SLIDER_X_OFFSET_REG_KEY = "BSC::SliderXOffset";
        private const string SLIDER_Y_OFFSET_REG_KEY = "BSC::SliderYOffset";


        private const bool DEFAULT_ACTIVATED = true;
        private const bool DEFAULT_DISPLAY_SETTINGS_BUTTON = true;
        private const bool DEFAULT_DISPLAY_SLIDER = true;

        private const float DEFAULT_MIN_SPEED = 0.05f;
        private const float DEFAULT_MAX_SPEED = 2.5f;

        private const int DEFAULT_SHIFT_MODE = 1;

        private const float DEFAULT_SHIFT_FIXED_SPEED = 3f;
        private const float DEFAULT_SHIFT_FACTOR_SPEED = 2f;

        private const float DEFAULT_CAMERA_SPEED = 1f;
        private const float DEFAULT_WHEEL_SPEED_FACTOR = 1f;

        private const bool DEFAULT_USE_INTERTIA = true;
        private const float DEFAULT_INERTIA_LENGTH = 0.3f;


        private const KeyCode DEFAULT_FLY_KEY = KeyCode.None;

        private const SliderAnchor DEFAULT_SLIDER_ANCHOR = SliderAnchor.TopRight;

        private const float DEFAULT_SLIDER_X_OFFSET = 30;
        private const float DEFAULT_SLIDER_Y_OFFSET = 5;

        private static bool _prefsLoaded = false;

        public static bool PrefsLoaded { get { return _prefsLoaded; } }

        public static bool Activated { get; private set; }
        public static bool DisplaySettingsButton { get; private set; }
        public static bool DisplaySlider { get; private set; }

        public static float NormalMinSpeed { get; private set; }
        public static float NormalMaxSpeed { get; private set; }

        public static ShiftMode ShiftPressedMode { get; private set; }

        public static float ShiftFixedSpeed { get; private set; }
        public static float ShiftFactorSpeed { get; private set; }

        public static float CurrentCameraSpeed { get; private set; }
        public static float WheelSpeedFactor { get; private set; }

        public static bool UseInertia { get; private set; }
        public static float InertiaLength { get; private set; }

        public static float GetInertiaFactor()
        {
            return Mathf.Lerp(3f, .2f, InertiaLength);
        }


        public static KeyCode Fly_Forward_Key { get; private set; }
        public static KeyCode Fly_Backward_Key { get; private set; }
        public static KeyCode Fly_Right_Key { get; private set; }
        public static KeyCode Fly_Left_Key { get; private set; }
        public static KeyCode Fly_Up_Key { get; private set; }
        public static KeyCode Fly_Down_Key { get; private set; }

        public static SliderAnchor Slider_Anchor { get; private set; }

        public static float Slider_XOffset { get; private set; }
        public static float Slider_YOffset { get; private set; }



        public static float CameraShiftSpeed
        {
            get
            {
                if (ShiftPressedMode == ShiftMode.Factor)
                {
                    return CurrentCameraSpeed * ShiftFactorSpeed;
                }
                else if (ShiftPressedMode == ShiftMode.Fixed)
                {
                    return ShiftFixedSpeed;
                }

                return 0;
            }
        }


        public static void Initialize()
        {
            _prefsLoaded = true;

            Activated = EditorPrefs.GetBool(ACTIVATED_REG_KEY, DEFAULT_ACTIVATED);
            DisplaySettingsButton = EditorPrefs.GetBool(DISPLAY_SETTINGS_BUTTON_REG_KEY, DEFAULT_DISPLAY_SETTINGS_BUTTON);
            DisplaySlider = EditorPrefs.GetBool(DISPLAY_SLIDER_REG_KEY, DEFAULT_DISPLAY_SETTINGS_BUTTON);

            NormalMinSpeed = EditorPrefs.GetFloat(MIN_SPEED_REG_KEY, DEFAULT_MIN_SPEED);
            NormalMaxSpeed = EditorPrefs.GetFloat(MAX_SPEED_REG_KEY, DEFAULT_MAX_SPEED);

            ShiftPressedMode = (ShiftMode)EditorPrefs.GetInt(SHIFT_MODE_REG_KEY, 0);

            ShiftFixedSpeed = EditorPrefs.GetFloat(SHIFT_FIXED_REG_KEY, DEFAULT_SHIFT_FIXED_SPEED);
            ShiftFactorSpeed = EditorPrefs.GetFloat(SHIFT_FACTOR_REG_KEY, DEFAULT_SHIFT_FACTOR_SPEED);

            CurrentCameraSpeed = EditorPrefs.GetFloat(CAMERA_SPEED_REG_KEY, DEFAULT_CAMERA_SPEED);
            WheelSpeedFactor = EditorPrefs.GetFloat(WHEEL_SPEED_FACTOR_REG_KEY, DEFAULT_WHEEL_SPEED_FACTOR);

            UseInertia = EditorPrefs.GetBool(USE_INERTIA_REG_KEY, DEFAULT_USE_INTERTIA);
            InertiaLength = EditorPrefs.GetFloat(INERTIA_LENGTH_REG_KEY, DEFAULT_INERTIA_LENGTH);

            Fly_Forward_Key = (KeyCode)EditorPrefs.GetInt(FLY_FORWARD_REG_KEY, (int)DEFAULT_FLY_KEY);
            Fly_Backward_Key = (KeyCode)EditorPrefs.GetInt(FLY_BACKWARD_REG_KEY, (int)DEFAULT_FLY_KEY);
            Fly_Right_Key = (KeyCode)EditorPrefs.GetInt(FLY_RIGHT_REG_KEY, (int)DEFAULT_FLY_KEY);
            Fly_Left_Key = (KeyCode)EditorPrefs.GetInt(FLY_LEFT_REG_KEY, (int)DEFAULT_FLY_KEY);
            Fly_Up_Key = (KeyCode)EditorPrefs.GetInt(FLY_UP_REG_KEY, (int)DEFAULT_FLY_KEY);
            Fly_Down_Key = (KeyCode)EditorPrefs.GetInt(FLY_DOWN_REG_KEY, (int)DEFAULT_FLY_KEY);

            Slider_Anchor = (SliderAnchor)EditorPrefs.GetInt(SLIDER_ANCHOR_REG_KEY, (int)DEFAULT_SLIDER_ANCHOR);

            Slider_XOffset = EditorPrefs.GetFloat(SLIDER_X_OFFSET_REG_KEY, DEFAULT_SLIDER_X_OFFSET);
            Slider_YOffset = EditorPrefs.GetFloat(SLIDER_Y_OFFSET_REG_KEY, DEFAULT_SLIDER_Y_OFFSET);
        }

        [PreferenceItem("BSC")]
        public static void PreferencesGUI()
        {
            if (_prefsLoaded == false)
            {
                Initialize();
            }
            GUIStyle titleStyle = new GUIStyle(GUI.skin.label);
            titleStyle.fontStyle = FontStyle.Bold;

            GUILayout.Label("Better Scene Camera", titleStyle);
            GUILayout.Space(8);

            EditorGUI.BeginChangeCheck();
            Activated = EditorGUILayout.Toggle("BSC Activated", Activated);
            if (EditorGUI.EndChangeCheck())
            {
                SceneView.lastActiveSceneView.Repaint();
                EditorPrefs.SetBool(ACTIVATED_REG_KEY, Activated);
            }

            GUILayout.Space(24);

            EditorGUI.BeginChangeCheck();

            GUIContent minSpeedGUIContent = new GUIContent("Minimum Speed", "Minimum speed of the camera");
            NormalMinSpeed = EditorGUILayout.FloatField(minSpeedGUIContent, NormalMinSpeed);
            NormalMaxSpeed = EditorGUILayout.FloatField("Maximum Speed", NormalMaxSpeed);
            if (EditorGUI.EndChangeCheck())
            {
                NormalMinSpeed = Mathf.Clamp(NormalMinSpeed, 0f, Mathf.Infinity);
                NormalMaxSpeed = Mathf.Clamp(NormalMaxSpeed, 0f, Mathf.Infinity);

                EditorPrefs.SetFloat(MIN_SPEED_REG_KEY, NormalMinSpeed);
                EditorPrefs.SetFloat(MAX_SPEED_REG_KEY, NormalMaxSpeed);
            }

            GUILayout.Space(12);

            EditorGUI.BeginChangeCheck();

            WheelSpeedFactor = EditorGUILayout.FloatField("Wheel Speed Factor", WheelSpeedFactor);
            if (EditorGUI.EndChangeCheck())
            {
                WheelSpeedFactor = Mathf.Clamp(WheelSpeedFactor, 0f, Mathf.Infinity);

                EditorPrefs.SetFloat(WHEEL_SPEED_FACTOR_REG_KEY, WheelSpeedFactor);
            }

            GUILayout.Space(12);


            EditorGUI.BeginChangeCheck();
            ShiftPressedMode = (ShiftMode)EditorGUILayout.EnumPopup("Shift Pressed Speed Mode", ShiftPressedMode);

            switch (ShiftPressedMode)
            {
                case ShiftMode.Fixed:
                    ShiftFixedSpeed = EditorGUILayout.FloatField("Shift Fixed Speed", ShiftFixedSpeed);
                    break;
                case ShiftMode.Factor:
                    ShiftFactorSpeed = EditorGUILayout.FloatField("Shift Speed Factor", ShiftFactorSpeed);
                    break;
            }

            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetInt(SHIFT_MODE_REG_KEY, (int)ShiftPressedMode);

                ShiftFixedSpeed = Mathf.Clamp(ShiftFixedSpeed, 0f, Mathf.Infinity);
                ShiftFactorSpeed = Mathf.Clamp(ShiftFactorSpeed, 0f, Mathf.Infinity);
                EditorPrefs.SetFloat(SHIFT_FIXED_REG_KEY, ShiftFixedSpeed);
                EditorPrefs.SetFloat(SHIFT_FACTOR_REG_KEY, ShiftFactorSpeed);
            }
            GUILayout.Space(12);

            EditorGUI.BeginChangeCheck();

            UseInertia = EditorGUILayout.Toggle("Use Inertia", UseInertia);
            if (UseInertia == true)
            {
                InertiaLength = EditorGUILayout.Slider("Inertia Length", InertiaLength, 0, 1f);
            }

            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetBool(USE_INERTIA_REG_KEY, UseInertia);
                EditorPrefs.SetFloat(INERTIA_LENGTH_REG_KEY, InertiaLength);
            }

            GUILayout.Space(24);

            GUILayout.Label("Visual", titleStyle);


            EditorGUI.BeginChangeCheck();
            DisplaySlider = EditorGUILayout.Toggle("Display Slider", DisplaySlider);
            if (EditorGUI.EndChangeCheck())
            {
                SceneView.lastActiveSceneView.Repaint();
                EditorPrefs.SetBool(DISPLAY_SLIDER_REG_KEY, DisplaySlider);
            }
            if (DisplaySlider == true)
            {
                EditorGUI.BeginChangeCheck();
                Slider_Anchor = (SliderAnchor)EditorGUILayout.EnumPopup("Slider Anchor", Slider_Anchor);
                if (EditorGUI.EndChangeCheck())
                {
                    SceneView.RepaintAll();
                    EditorPrefs.SetInt(SLIDER_ANCHOR_REG_KEY, (int)Slider_Anchor);
                }

                EditorGUI.BeginChangeCheck();
                Slider_XOffset = EditorGUILayout.FloatField("X Offset", Slider_XOffset);
                Slider_YOffset = EditorGUILayout.FloatField("Y Offset", Slider_YOffset);
                if (EditorGUI.EndChangeCheck())
                {
                    SceneView.RepaintAll();
                    EditorPrefs.SetFloat(SLIDER_X_OFFSET_REG_KEY, Slider_XOffset);
                    EditorPrefs.SetFloat(SLIDER_Y_OFFSET_REG_KEY, Slider_YOffset);
                }

                GUILayout.Space(4);


                EditorGUI.BeginChangeCheck();
                DisplaySettingsButton = EditorGUILayout.Toggle("Display Settings Button", DisplaySettingsButton);
                if (EditorGUI.EndChangeCheck())
                {
                    SceneView.lastActiveSceneView.Repaint();
                    EditorPrefs.SetBool(DISPLAY_SETTINGS_BUTTON_REG_KEY, DisplaySettingsButton);
                }
            }


            GUILayout.Space(24);

            GUILayout.Label("Key Bindings", titleStyle);
            GUILayout.Label("If none is set, BSC will try to read your editor prefs but,\nsometimes, it fails due to Unity not giving us access to\nuser custom shortcuts easily.");
            GUILayout.Space(12);

            EditorGUI.BeginChangeCheck();

            Fly_Forward_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Forward Key", Fly_Forward_Key);
            Fly_Backward_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Backward Key", Fly_Backward_Key);
            Fly_Right_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Right Key", Fly_Right_Key);
            Fly_Left_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Left Key", Fly_Left_Key);
            Fly_Up_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Up Key", Fly_Up_Key);
            Fly_Down_Key = (KeyCode)EditorGUILayout.EnumPopup("Fly Down Key", Fly_Down_Key);

            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetInt(FLY_FORWARD_REG_KEY, (int)Fly_Forward_Key);
                EditorPrefs.SetInt(FLY_BACKWARD_REG_KEY, (int)Fly_Backward_Key);
                EditorPrefs.SetInt(FLY_RIGHT_REG_KEY, (int)Fly_Right_Key);
                EditorPrefs.SetInt(FLY_LEFT_REG_KEY, (int)Fly_Left_Key);
                EditorPrefs.SetInt(FLY_UP_REG_KEY, (int)Fly_Up_Key);
                EditorPrefs.SetInt(FLY_DOWN_REG_KEY, (int)Fly_Down_Key);
            }
        }

        public static void OnDisable()
        {
            Activated = false;

            EditorPrefs.SetBool(ACTIVATED_REG_KEY, Activated);
        }

        public static void OnEnable()
        {
            Activated = true;

            EditorPrefs.SetBool(ACTIVATED_REG_KEY, Activated);
        }

        public static void SetCurrentSpeed(float speed)
        {
            speed = Mathf.Clamp(speed, NormalMinSpeed, NormalMaxSpeed);

            if (speed != CurrentCameraSpeed)
            {
                CurrentCameraSpeed = speed;
                EditorPrefs.SetFloat(CAMERA_SPEED_REG_KEY, CurrentCameraSpeed);
            }
        }

        public static void SetMinSpeed(float speed)
        {
            speed = Mathf.Clamp(speed, 0f, NormalMaxSpeed);

            if (speed != NormalMinSpeed)
            {
                NormalMinSpeed = speed;
                EditorPrefs.SetFloat(MIN_SPEED_REG_KEY, NormalMinSpeed);
            }
        }

        public static void SetMaxSpeed(float speed)
        {
            speed = Mathf.Clamp(speed, NormalMinSpeed, Mathf.Infinity);

            if (speed != NormalMaxSpeed)
            {
                NormalMaxSpeed = speed;
                EditorPrefs.SetFloat(MAX_SPEED_REG_KEY, NormalMaxSpeed);
            }
        }

        public static void SetWheelSpeedFactor(float speed)
        {
            speed = Mathf.Clamp(speed, 0f, Mathf.Infinity);

            if (speed != WheelSpeedFactor)
            {
                WheelSpeedFactor = speed;
                EditorPrefs.SetFloat(WHEEL_SPEED_FACTOR_REG_KEY, WheelSpeedFactor);
            }
        }

        public static void SetSliderAnchor(SliderAnchor anchor)
        {
            if (anchor != Slider_Anchor)
            {
                Slider_Anchor = anchor;
                EditorPrefs.SetInt(SLIDER_ANCHOR_REG_KEY, (int)Slider_Anchor);
                SceneView.RepaintAll();
            }
        }
    }
}
