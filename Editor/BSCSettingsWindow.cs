﻿/*Made by Eostream : Michaël Cossu - 17-12-2018*/
using UnityEngine;
using UnityEditor;

namespace BSC
{
    public class BSCSettingsWindow : EditorWindow
    {
        private static BSCSettingsWindow _instance;
        private static bool _opened = false;

        public static void OpenClose(Rect rectToSnap)
        {
            _instance = CreateInstance<BSCSettingsWindow>();

            Rect rect = new Rect(rectToSnap);
            switch (BSCSettings.Slider_Anchor)
            {
                case SliderAnchor.TopRight:
                    rect.x -= 200 - rectToSnap.width;
                    break;
                case SliderAnchor.TopLeft:
                    break;
                case SliderAnchor.BottomLeft:
                    rect.y -= 120 - rectToSnap.height;
                    break;
                case SliderAnchor.BottomRight:
                    rect.x -= 200 - rectToSnap.width;
                    rect.y -= 120 - rectToSnap.height;
                    break;
                default:
                    break;
            }

            _instance.ShowAsDropDown(rect, new Vector2(200, 120));
        }

        private void OnGUI()
        {
            GUILayout.Label("Min & Max speed settings", EditorStyles.boldLabel);
            BSCSettings.SetMinSpeed(EditorGUILayout.FloatField("Min Speed", BSCSettings.NormalMinSpeed));
            BSCSettings.SetMaxSpeed(EditorGUILayout.FloatField("Max Speed", BSCSettings.NormalMaxSpeed));
            BSCSettings.SetCurrentSpeed(EditorGUILayout.FloatField("Current Speed", BSCSettings.CurrentCameraSpeed));
            GUILayout.Space(5);
            BSCSettings.SetWheelSpeedFactor(EditorGUILayout.FloatField("Wheel Speed Factor", BSCSettings.WheelSpeedFactor));
            if (GUILayout.Button("Close", EditorStyles.centeredGreyMiniLabel))
            {
                _instance.Close();
                _instance = null;
            }
            SceneView.lastActiveSceneView.Repaint();
        }
    }
}
