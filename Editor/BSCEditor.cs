﻿/*Made by Eostream : Michaël Cossu - 17-12-2018*/
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.AnimatedValues;
using UnityEditor;
using System.Text;

namespace BSC
{
    public enum SliderAnchor
    {
        TopRight,
        TopLeft,
        BottomLeft,
        BottomRight
    }

    [InitializeOnLoad]
    public static class BSCEditor
    {
        public enum MoveDirection
        {
            Forward,
            Backward,
            Right,
            Left,
            Up,
            Down
        }

        static BSCEditor()
        {
            OnEnable();
        }

        private const float SLIDER_WIDTH = 100;
        private const float SLIDER_HEIGHT = 20;
        private const float TOP_RIGHT_EXTRA_PADDING = 80;
        private const float BOTTOM_EXTRA_PADDING = 35;

        private const string FORWARD_KEY_NAME = "3D Viewport/Fly Mode Forward";
        private const string BACKWARD_KEY_NAME = "3D Viewport/Fly Mode Backward";
        private const string RIGHT_KEY_NAME = "3D Viewport/Fly Mode Right";
        private const string LEFT_KEY_NAME = "3D Viewport/Fly Mode Left";
        private const string UP_KEY_NAME = "3D Viewport/Fly Mode Up";
        private const string DOWN_KEY_NAME = "3D Viewport/Fly Mode Down";

        private static bool _initialized;
        private static bool _overHandle = false;
        private static bool _handlePressed = false;
        private static bool _rightMousePressed = false;
        private static bool _forwardPressed = false;
        private static bool _backwardPressed = false;
        private static bool _rightPressed = false;
        private static bool _leftPressed = false;
        private static bool _upPressed = false;
        private static bool _downPressed = false;
        private static bool _shiftPressed = false;

        private static float _timeAtLastUpdate;
        private static float _deltaTime;
        private static float _lastMoveSpeed;

        private static string _currentKeyName;

        private static Vector3 _cameraPos;
        private static Quaternion _cameraRot;

        private static Transform _sceneCam;
        private static SceneView _GUISceneView;
        private static SceneView _sliderDraggingSceneView;
        private static SceneView _rightClickedSceneView;
        private static SceneView _lastUpdateSceneView;

        private static Vector3 _moveInputs;
        private static Vector3 _velocity;

        private static Texture _settingsIcon;
        private static Texture _handleIcon;

        private static Dictionary<string, KeyCode> customInputsDic;

        //Reflection field used to get the sceneview animVector component
        private static System.Reflection.BindingFlags nonPublicFlags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
        private static AnimVector3 _sceneViewAnimVector;
        private static System.Reflection.FieldInfo _isRightMousePressedForUnitySceneView_FieldReference;


        public static Transform SceneCam { get { return _sceneCam; } }


        [MenuItem("Tools/BSC/Start")]
        private static void OnEnable()
        {
            //Activate settings class
            BSCSettings.Initialize();
            BSCSettings.OnEnable();

            //Initialize in next OnSceneGUI() Event
            _initialized = false;

            //Check the custom editor shortcuts
            InitializeCustomShortcuts();

            //Listen for events
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            EditorApplication.update -= Update;
            EditorApplication.update += Update;

            //Delta time handler
            _timeAtLastUpdate = (float)EditorApplication.timeSinceStartup;
            _deltaTime = 0f;
        }

        private static void InitializeCustomShortcuts()
        {
            //BruteForce method lol if you have better suggestions i'm opened
            string[] path = Application.persistentDataPath.Split('/');
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < path.Length; i++)
            {
                if (path[i] == "LocalLow")
                {
                    break;
                }
                else
                {
                    builder.Append(path[i]);
                    builder.Append("/");
                }
            }
            builder.Append("Roaming/Unity/Editor-5.x/Preferences/shortcuts/UserProfile.shortcut");
            FileStream stream;
            try
            {
                stream = new FileStream(builder.ToString(), FileMode.OpenOrCreate, FileAccess.Read);
            }
            catch
            {
                //Path incorrect
                customInputsDic = new Dictionary<string, KeyCode>();
                return;
            }

            StreamReader reader = new StreamReader(stream);
            string machin = reader.ReadToEnd();

            char[] separator = new char[] { '"', ':', '{', '}', '[', ']', ',' };
            string[] rawData = machin.Split(separator);
            List<string> data = new List<string>();
            customInputsDic = new Dictionary<string, KeyCode>();

            string currentPath = "";
            bool willStorePath = false;
            bool hasStoredLastData = false;
            bool willStoreKey = false;
            for (int i = 0; i < rawData.Length; i++)
            {
                if (rawData[i].Length > 0)
                {
                    if (rawData[i] == "path")
                    {
                        willStorePath = true;
                    }
                    else if (willStorePath == true)
                    {
                        if (rawData[i].Contains("Fly Mode") == false)
                        {
                            currentPath = rawData[i];
                            willStorePath = false;
                            continue;
                        }
                        else
                        {
                            currentPath = rawData[i];
                            willStorePath = false;
                            hasStoredLastData = true;
                        }
                    }


                    if (hasStoredLastData == true && rawData[i] == "m_KeyCode")
                    {
                        hasStoredLastData = false;
                        willStoreKey = true;
                    }
                    else if (willStoreKey == true)
                    {
                        customInputsDic.Add(currentPath, (KeyCode)System.Enum.Parse(typeof(KeyCode), rawData[i]));
                        willStoreKey = false;
                    }
                }
            }
        }


        private static void OnSceneGUI(SceneView sceneView)
        {
            if (BSCSettings.Activated == true)
            {
                Init(sceneView);

                if (_GUISceneView.in2DMode == true)
                {
                    return;
                }

                if (BSCSettings.DisplaySlider == true)
                {
                    DrawSlider();
                    UpdateMouseDrag();
                }

                CheckRightMouse(sceneView);

                if (_rightMousePressed == true)
                {
                    LockCamera();
                    CheckShift();
                    UpdateCurrentKeyName();
                    UpdateMoveInput();
                }
                else
                {
                    MouseWheelMovement();
                }
            }
        }

        private static void Init(SceneView scene)
        {
            if (scene != _GUISceneView)
            {
                _GUISceneView = scene;
                _sceneCam = _GUISceneView.camera.transform;

                _sceneViewAnimVector = ((AnimVector3)_GUISceneView.GetType().GetField("m_Position", nonPublicFlags).GetValue(_GUISceneView));
                _isRightMousePressedForUnitySceneView_FieldReference = _GUISceneView.GetType().GetField("s_DraggingCursorIsCached", nonPublicFlags);

            }

            if (_initialized == false)
            {
                _settingsIcon = Resources.Load<Texture>("BSC\\SettingsIcon");
                _handleIcon = Resources.Load<Texture>("BSC\\HandleIcon");

                _initialized = true;
            }
        }
        private static void DrawSlider()
        {
            Handles.BeginGUI();
            GUIStyle sliderStyle = GUI.skin.horizontalSlider;
            GUIStyle thumbStyle = GUI.skin.horizontalSliderThumb;

            Rect sliderRect;
            float x = BSCSettings.Slider_XOffset;
            float y = BSCSettings.Slider_YOffset;
            switch (BSCSettings.Slider_Anchor)
            {
                case SliderAnchor.TopLeft:
                sliderRect = new Rect(x, y, SLIDER_WIDTH, SLIDER_HEIGHT);
                    break;
                case SliderAnchor.BottomLeft:
                sliderRect = new Rect(x, Screen.height - y - SLIDER_HEIGHT - BOTTOM_EXTRA_PADDING, SLIDER_WIDTH, SLIDER_HEIGHT);
                    break;
                case SliderAnchor.BottomRight:
                sliderRect = new Rect(Screen.width - x - SLIDER_WIDTH, Screen.height - y - SLIDER_HEIGHT - BOTTOM_EXTRA_PADDING, SLIDER_WIDTH, SLIDER_HEIGHT);
                    break;
                case SliderAnchor.TopRight:
                default:
                sliderRect = new Rect(Screen.width - x - SLIDER_WIDTH - TOP_RIGHT_EXTRA_PADDING, y, SLIDER_WIDTH, SLIDER_HEIGHT);
                    break;
            }
            BSCSettings.SetCurrentSpeed(GUI.HorizontalSlider(
                sliderRect,
                BSCSettings.CurrentCameraSpeed,
                BSCSettings.NormalMinSpeed,
                BSCSettings.NormalMaxSpeed,
                sliderStyle,
                thumbStyle));

            Rect rightRect = new Rect()
            {
                width = SLIDER_HEIGHT * 0.75f,
                height = SLIDER_HEIGHT * 0.75f,
                x = sliderRect.x + SLIDER_WIDTH,
                y = sliderRect.y + 3
            };
            Rect leftRect = new Rect()
            {
                width = SLIDER_HEIGHT * 0.75f,
                height = SLIDER_HEIGHT * 0.75f,
                x = sliderRect.x - SLIDER_HEIGHT * 0.75f,
                y = sliderRect.y + 3
            };
            Rect handleRect;
            Rect settingsRect;
            switch (BSCSettings.Slider_Anchor)
            {
                case SliderAnchor.BottomRight:
                case SliderAnchor.TopRight:
                default:
                    handleRect = rightRect;
                    settingsRect = leftRect;
                    settingsRect.x -= 5;
                    break;
                case SliderAnchor.TopLeft:
                case SliderAnchor.BottomLeft:
                    handleRect = leftRect;
                    settingsRect = rightRect;
                    settingsRect.x += 5;
                    break;
            }

            GUI.Label(handleRect, _handleIcon, GUI.skin.label);
            GUI.Label(handleRect, new GUIContent("", "Drag to screen corners to move slider's anchor"), GUI.skin.label);
            EditorGUIUtility.AddCursorRect(handleRect, MouseCursor.MoveArrow);

            if (_handlePressed == false)
            {
                if (handleRect.Contains(Event.current.mousePosition))
                {
                    HandleUtility.AddDefaultControl(GUIUtility.GetControlID(0, FocusType.Passive));

                    if (Event.current.button == 0 && Event.current.type == EventType.MouseDown)
                    {
                        _handlePressed = true;
                        _sliderDraggingSceneView = _GUISceneView;
                        _GUISceneView.Focus();
                    }
                }
            }
            
            if (_handlePressed == true)
            {
                int controlID = GUIUtility.GetControlID(0, FocusType.Passive);

                switch (Event.current.type)
                {
                    case EventType.MouseUp:
                        _handlePressed = false;
                        Event.current.Use();
                        break;

                    case EventType.Layout:
                        HandleUtility.AddDefaultControl(controlID);
                        break;
                }
            }

            if (BSCSettings.DisplaySettingsButton == true)
            {
                if (GUI.Button(settingsRect, _settingsIcon, GUI.skin.label))
                {
                    settingsRect.x += _GUISceneView.position.x;
                    settingsRect.y += _GUISceneView.position.y;
                    BSCSettingsWindow.OpenClose(settingsRect);
                }
            }


            Handles.EndGUI();
        }
        private static void UpdateMouseDrag()
        {
            if (_handlePressed == false || Event.current.type == EventType.Layout)
            {
                return;
            }
            if (_GUISceneView != _sliderDraggingSceneView)
            {
                return;
            }

            EditorGUIUtility.AddCursorRect(new Rect(0, 0, Screen.width, Screen.height), MouseCursor.ScaleArrow);

            Vector2[] screenMaxPos = new Vector2[]
            {
                new Vector2(Screen.width, 0f),
                new Vector2(0f, 0f),
                new Vector2(0f, Screen.height),
                new Vector2(Screen.width, Screen.height)
            };

            int curIndex = 0;
            float curDistance = Vector2.Distance(Event.current.mousePosition, screenMaxPos[0]);
            for (int i = 1; i < screenMaxPos.Length; i++)
            {
                if (Vector2.Distance(Event.current.mousePosition, screenMaxPos[i]) < curDistance)
                {
                    curIndex = i;
                    curDistance = Vector2.Distance(Event.current.mousePosition, screenMaxPos[i]);
                }
            }

            BSCSettings.SetSliderAnchor((SliderAnchor)curIndex);
        }
        private static void CheckRightMouse(SceneView view)
        {
            if (Event.current.button != 1)
            {
                return;
            }

            if (Event.current.type == EventType.MouseDown)
            {
                _rightClickedSceneView = view;
                _rightMousePressed = true;
            }
            else if (Event.current.type == EventType.MouseUp)
            {
                OnRightMouseUp();
            }
        }
        private static void OnRightMouseUp()
        {
            _rightMousePressed = false;

            _forwardPressed = false;
            _backwardPressed = false;
            _rightPressed = false;
            _leftPressed = false;
            _upPressed = false;
            _downPressed = false;
            _shiftPressed = false;
        }
        private static void LockCamera()
        {
            if (Event.current.type == EventType.KeyDown)
            {
                Event.current.Use();
            }
            else if (Event.current.type == EventType.ScrollWheel)
            {
                Event.current.Use();

                BSCSettings.SetCurrentSpeed(
                    BSCSettings.CurrentCameraSpeed -
                    (Event.current.delta.y * BSCSettings.CurrentCameraSpeed) * (5/60f) * BSCSettings.WheelSpeedFactor);
            }
        }
        private static void CheckShift()
        {
            if (Event.current.modifiers == EventModifiers.Shift)
            {
                _shiftPressed = true;
            }
        }
        private static void UpdateCurrentKeyName()
        {
            if (Event.current.keyCode != KeyCode.None)
            {
                if (Event.current.keyCode == BSCSettings.Fly_Forward_Key)
                {
                    _currentKeyName = FORWARD_KEY_NAME;
                }
                else if (Event.current.keyCode == BSCSettings.Fly_Backward_Key)
                {
                    _currentKeyName = BACKWARD_KEY_NAME;
                }
                else if (Event.current.keyCode == BSCSettings.Fly_Right_Key)
                {
                    _currentKeyName = RIGHT_KEY_NAME;
                }
                else if (Event.current.keyCode == BSCSettings.Fly_Left_Key)
                {
                    _currentKeyName = LEFT_KEY_NAME;
                }
                else if (Event.current.keyCode == BSCSettings.Fly_Up_Key)
                {
                    _currentKeyName = UP_KEY_NAME;
                }
                else if (Event.current.keyCode == BSCSettings.Fly_Down_Key)
                {
                    _currentKeyName = DOWN_KEY_NAME;
                }
                else if (customInputsDic.ContainsValue(Event.current.keyCode))
                {
                    foreach (string key in customInputsDic.Keys)
                    {
                        if (customInputsDic[key] == Event.current.keyCode)
                        {
                            _currentKeyName = key;
                            break;
                        }
                    }
                }
                else
                {
                    _currentKeyName = GetDefaultKeyName(Event.current.keyCode);
                }

                UpdateUpDownForKeyName(_currentKeyName, Event.current.type != EventType.KeyUp);
            }
        }
        private static void UpdateUpDownForKeyName(string keyName, bool down)
        {
            if (_currentKeyName == FORWARD_KEY_NAME)
            {
                _forwardPressed = down;
            }
            if (_currentKeyName == BACKWARD_KEY_NAME)
            {
                _backwardPressed = down;
            }
            if (_currentKeyName == UP_KEY_NAME)
            {
                _upPressed = down;
            }
            if (_currentKeyName == DOWN_KEY_NAME)
            {
                _downPressed = down;
            }
            if (_currentKeyName == RIGHT_KEY_NAME)
            {
                _rightPressed = down;
            }
            if (_currentKeyName == LEFT_KEY_NAME)
            {
                _leftPressed = down;
            }

            _currentKeyName = "";
        }
        private static void UpdateMoveInput()
        {
            if (_forwardPressed == true && _backwardPressed == false)
            {
                _moveInputs.z = 1;
            }
            else if (_forwardPressed == false && _backwardPressed == true)
            {
                _moveInputs.z = -1;
            }
            else
            {
                _moveInputs.z = 0;
            }

            if (_rightPressed == true && _leftPressed == false)
            {
                _moveInputs.x = 1;
            }
            else if (_rightPressed == false && _leftPressed == true)
            {
                _moveInputs.x = -1;
            }
            else
            {
                _moveInputs.x = 0;
            }

            if (_upPressed == true && _downPressed == false)
            {
                _moveInputs.y = 1;
            }
            else if (_upPressed == false && _downPressed == true)
            {
                _moveInputs.y = -1;
            }
            else
            {
                _moveInputs.y = 0;
            }
        }
        private static void MouseWheelMovement()
        {
            if (Event.current.type == EventType.ScrollWheel)
            {
                Event.current.Use();

                _cameraPos += _sceneCam.TransformVector(Vector3.forward * BSCSettings.CurrentCameraSpeed * Event.current.delta.y * -0.2f);
                _sceneViewAnimVector.value = _cameraPos;
            }
        }


        private static void Update()
        {
            if (BSCSettings.Activated == true)
            {
                Init(SceneView.lastActiveSceneView);
                PreventInertiaShare();
                if (_sceneCam == null)
                {
                    return;
                }
                if (_GUISceneView.in2DMode == true)
                {
                    return;
                }

                UpdateDeltaTime();

                UltimateRightMouseCheck();
                ComputeInputs();
                HandleVelocity();
                HandleCameraMovements();

                _shiftPressed = false;
            }
        }
        private static void PreventInertiaShare()
        {
            if (SceneView.lastActiveSceneView != _lastUpdateSceneView)
            {
                _velocity = Vector3.zero;
            }
            _lastUpdateSceneView = SceneView.lastActiveSceneView;
        }
        private static void UpdateDeltaTime()
        {
            _deltaTime = ((float)EditorApplication.timeSinceStartup - _timeAtLastUpdate) * 5f;
            _timeAtLastUpdate = (float)EditorApplication.timeSinceStartup;
        }
        private static void UltimateRightMouseCheck()
        {
            //If the user has released right click outside scene view, this is the only way to get it lmfao.
            if (EditorWindow.mouseOverWindow != _GUISceneView && ((bool)_isRightMousePressedForUnitySceneView_FieldReference.GetValue(_GUISceneView) == false) && _rightMousePressed == true)
            {
                OnRightMouseUp();
            }
        }
        private static void ComputeInputs()
        {
            if (_shiftPressed == true)
            {
                _moveInputs = _moveInputs.normalized * BSCSettings.CameraShiftSpeed;
                if (_moveInputs.sqrMagnitude > 0)
                {
                    _lastMoveSpeed = BSCSettings.CameraShiftSpeed;
                }
            }
            else
            {
                _moveInputs = _moveInputs.normalized * BSCSettings.CurrentCameraSpeed;
                if (_moveInputs.sqrMagnitude > 0)
                {
                    _lastMoveSpeed = BSCSettings.CurrentCameraSpeed;
                }
            }
        }
        private static void HandleVelocity()
        {
            if (_moveInputs.sqrMagnitude > 0)
            {
                _velocity = _moveInputs;
            }
            else
            {
                if (BSCSettings.UseInertia == true)
                {
                    float magn = Mathf.Clamp((_velocity.magnitude - _deltaTime * BSCSettings.GetInertiaFactor() * _lastMoveSpeed), 0, Mathf.Infinity);
                    _velocity = _velocity.normalized * magn;
                }
                else
                {
                    _velocity = Vector3.zero;
                }
            }
        }
        private static void HandleCameraMovements()
        {
            if (_sceneViewAnimVector.target != _cameraPos)
            {
                _cameraPos = _sceneViewAnimVector.target;
            }

            if (_sceneViewAnimVector.isAnimating == false)
            {
                _cameraPos += _sceneCam.TransformVector(_velocity * _deltaTime);
                _sceneViewAnimVector.value = _cameraPos;
            }

            _moveInputs = Vector3.zero;
        }

        [MenuItem("Tools/BSC/Stop")]
        private static void OnDisable()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            SceneView.RepaintAll();

            BSCSettings.OnDisable();
        }

        //Utilities
        private static string GetDefaultKeyName(KeyCode key)
        {
            switch (key)
            {
                case KeyCode.W:
                    return FORWARD_KEY_NAME;
                case KeyCode.S:
                    return BACKWARD_KEY_NAME;
                case KeyCode.D:
                    return RIGHT_KEY_NAME;
                case KeyCode.A:
                    return LEFT_KEY_NAME;
                case KeyCode.E:
                    return UP_KEY_NAME;
                case KeyCode.Q:
                    return DOWN_KEY_NAME;
            }

            return null;
        }

    }
}
